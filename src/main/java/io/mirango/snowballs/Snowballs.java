package io.mirango.snowballs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

public class Snowballs extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "Snowballs enabled. Version: " + getDescription().getVersion());
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @EventHandler(priority=EventPriority.NORMAL)
    public void onHit(ProjectileHitEvent event) {
        if(event.getEntity() instanceof Snowball && event.getHitEntity() instanceof Player) {
            Snowball s = (Snowball)event.getEntity();
            Vector sVelocity = s.getVelocity();

            Player shooter = (Player)s.getShooter();
            Player target = (Player)event.getHitEntity();

            shooter.sendMessage(ChatColor.DARK_BLUE + "Hit " + target.getDisplayName());
            target.sendMessage(ChatColor.DARK_RED + "Hit by " + shooter.getDisplayName());

            target.setVelocity(target.getVelocity().midpoint(sVelocity));
        }
    }


}
